
function FistLoad(){

    var _this = this;
    this.hideDotorsSlider = function(){
        $('.b-ind-doctors__slider').hide();
        $('.b-ind-doctors__slider[data-id="1"]').show()
    };
    this.run = function(){
        _this.hideDotorsSlider();
    }
}



$(document).ready(function(){
    $('.b-main-slider').slick({
        dots: true,
        prevArrow: '<span class="b-main-slider-arr b-main-slider-arr--prev"><span class="b-m-slide-arr__item"></span></span>',
        nextArrow: '<span class="b-main-slider-arr b-main-slider-arr--next"><span class="b-m-slide-arr__item"></span></span>',
        dotsClass: 'b-main-slider-dots'
    });

    var initDocSlider = function (){
        $('.b-ind-doctors-slider').slick({
            dots: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<span class="b-main-slider-arr b-main-slider-arr--prev b-main-slider-arr--prev--doc"><span class="b-m-slide-arr__item"></span></span>',
            nextArrow: '<span class="b-main-slider-arr b-main-slider-arr--next b-main-slider-arr--next--doc"><span class="b-m-slide-arr__item"></span></span>'
        });
    };

    initDocSlider();

    $('.b-ind-doctors-link-town').on('click', function(){
        $('.b-ind-doctors-link-town').removeClass('b-ind-doctors-link-town--check');
        $(this).addClass('b-ind-doctors-link-town--check');
        var id = $(this).attr('data-id');
        var parrent = $(this).parents('.b-index-doctors');
        $('.b-ind-doctors-slider').slick('unslick');
        parrent.find('.b-ind-doctors__slider').hide();
        parrent.find('.b-ind-doctors__slider[data-id="'+id+'"]').show();
        initDocSlider();
    });

    var load = new FistLoad();
    load.run();
});