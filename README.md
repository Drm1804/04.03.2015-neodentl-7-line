#Gulp for landing page

##Установка

- npm install
- bower install
- gulp

##Описание

Проект для стоматологии, поддерживает сборку и livereload


## Учет версий
Проект поддерживает  [Семантическое Версионирование 2.0.0](http://semver.org/lang/ru/).


## Авторы
 - [ Ustinov Alexey ](https://github.com/Drm1804)